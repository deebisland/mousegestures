from Xlib import display
import time
import os

data = display.Display().screen().root.query_pointer()._data
original_x, original_y = data["root_x"], data["root_y"]

time.sleep(0.28) # todo refactor into while true loop that waits for stop motion, with a timeout of 0.4s

data = display.Display().screen().root.query_pointer()._data
new_x, new_y = data["root_x"], data["root_y"]

try:
    is_x = abs(new_x - original_x) / abs(new_y - original_y) > 2
except:
    is_x = False
    
if not is_x:
    if new_y > original_y + 20: # down
        os.system('xdotool key Control_L+Shift_L+Page_Down')
    elif new_y < original_y - 20: # up
        os.system('xdotool key Control_L+Shift_L+Page_Up')
    else: # NO/minimal movement
        os.system('xdotool key Shift_L+Control_L+Super_L+KP_Subtract')
else:
    if new_x > original_x + 20: # right
        os.system('xdotool key Super_L+Alt_L+t')
    elif new_x < original_x - 20: # left
        os.system('xdotool key Shift_L+Control_L+Super_L+KP_Add')
    else: # no/minimal movement
        os.system('xdotool key Shift_L+Control_L+Super_L+KP_Subtract')

